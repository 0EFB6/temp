#!/bin/bash

pack_installer="dnf remove"

sudo ${pack_installer} gnome-calculator -y
sudo ${pack_installer} gnome-calendar -y
sudo ${pack_installer} gnome-characters -y
sudo ${pack_installer} cheese -y
sudo ${pack_installer} gnome-clocks -y
sudo ${pack_installer} gnome-connections -y
sudo ${pack_installer} gnome-contacts -y
sudo ${pack_installer} baobab -y
sudo ${pack_installer} simple-scan -y
sudo ${pack_installer} evince -y
sudo ${pack_installer} gnome-font-viewer -y
sudo ${pack_installer} gnome-color-manager -y
sudo ${pack_installer} yelp -y
sudo ${pack_installer} eog -y
sudo ${pack_installer} libreoffice-calc -y
sudo ${pack_installer} libreoffice-impress -y
sudo ${pack_installer} libreoffice-writer -y
sudo ${pack_installer} gnome-logs -y
sudo ${pack_installer} gnome-maps -y
sudo ${pack_installer} gnome-photos -y
sudo ${pack_installer} gnome-abrt -y
sudo ${pack_installer} rhythmbox -y
sudo ${pack_installer} gnome-text-editor -y
sudo ${pack_installer} gnome-tour -y
sudo ${pack_installer} totem -y
sudo ${pack_installer} gnome-weather -y
sudo ${pack_installer} ibus-anthy -y
sudo ${pack_installer} ibus-hangul -y
sudo ${pack_installer} ibus-m17n -y
sudo ${pack_installer} ibus-typing-booster -y
sudo killall gnome-software
